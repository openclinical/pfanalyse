/*
PFAnalyse is a node CLI program for analysing proformajs decisions
Copyright (C) 2018 - Openclinical CIC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
### NOTE

This is a start, and sets up a working test scaffold that can be used in CI
but there isnt yet sufficient test coverage.  Nine examples are suggested
in the README and these tests cover four of them and half of a further two.
The dyndef example isnt currently used and the processJSON function isnt loaded.
*/

import { Protocol } from "@openclinical/proformajs";
import fs from "fs";

import {
  loadUrl,
  getDecisionPaths,
  dependentDataDefs,
  dependentDecisions,
  extractDecision,
} from "./lib.js";
import chai from "chai";
chai.should(); // monkeypatch should function so its available in tests

const protocols = {};
for (var filename of fs.readdirSync("./etc")) {
  const splits = filename.split(".");
  if (splits[1] == "json") {
    // const json = JSON.parse(fs.readFileSync('./etc/' + filename, 'utf-8'));
    protocols[splits[0]] = Protocol.inflate(
      fs.readFileSync("./etc/" + filename, "utf-8"),
    );
  }
}

const logger = function (text, err) {
  if (text) {
    console.log(text);
  }
  if (err) {
    console.log(err);
  }
};

describe("loading protocols", () => {
  it("from file", (done) => {
    loadUrl("file://./etc/cold_or_flu.json", logger, (err, res) => {
      if (!err) {
        const json = JSON.parse(res);
        json.name.should.equal("cold_or_flu");
        done();
      } else {
        done(err);
      }
    });
  });
  it("from web", (done) => {
    loadUrl(
      "https://labs.openclinical.net/api/v1/demo/cold_or_flu",
      logger,
      (err, res) => {
        if (!err) {
          const json = JSON.parse(res);
          json.data.name.should.equal("cold_or_flu");
          done();
        } else {
          done(err);
        }
      },
    );
  });
  // TODO: implement this test without coding credentials
  it.skip("from web (with authentication)");
});

describe("processing test protocols", () => {
  it("cold_or_flu", () => {
    const decs = getDecisionPaths(protocols.cold_or_flu);
    decs.should.deep.equal(["cold_or_flu:diagnosis"]);
    const dds = dependentDataDefs(protocols.cold_or_flu.getComponent(decs[0]));
    dds.should.deep.equal(["complaints", "vaccine", "temperature", "cough"]);
    const dec = extractDecision(protocols.cold_or_flu, decs[0]);
    dec.name.should.equal("decision");
    // todo: some more here - check that extracted decision matches the original decision in all but:
    // * name
    // * paths to candidates in net_support expressions
    // * pre and wait conditions
    const orig = protocols.cold_or_flu.tasks[2];
    dec.caption.should.equal(orig.caption);
    dec.candidates.length.should.equal(orig.candidates.length);
  });
});

describe("including dependent decisions", () => {
  it("issue1", () => {
    const decs = getDecisionPaths(protocols.issue1);
    decs.should.deep.equal(["cold_or_flu:diagnosis", "cold_or_flu:diagnosis2"]);
    const dds = dependentDataDefs(protocols.issue1.getComponent(decs[1]));
    dds.should.deep.equal(["age"]);
    const ds = dependentDecisions(protocols.issue1.getComponent(decs[1]));
    ds.should.deep.equal(["cold_or_flu:diagnosis"]);
    const dec = extractDecision(protocols.issue1, decs[1]);
    dec.name.should.equal("decision");
    dec.isValid().should.be.true;
  });
});

describe("handling nested dynamic dds", () => {
  it("issue4", () => {
    const decs = getDecisionPaths(protocols.dd_name_clash);
    decs.should.deep.equal(["plan:decision"]);
    const dds = dependentDataDefs(protocols.dd_name_clash.getComponent(decs[0]));
    dds.should.deep.equal(["tews", "tews_adult", "tews_child", "tews_child_part"]);
    // const ds = dependentDecisions(protocols.issue1.getComponent(decs[1]));
    // ds.should.deep.equal(["cold_or_flu:diagnosis"]);
    // const dec = extractDecision(protocols.issue1, decs[1]);
    // dec.name.should.equal("decision");
    // dec.isValid().should.be.true;
  });
});
