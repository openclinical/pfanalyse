# PFAnalyse

PFAnalyse is a command-line tool for analysing PROformajs protocols.

## Getting started

With [Nodejs](https://nodejs.org) (tested with v16+) and [Git](https://git-scm.com/) installed you can first clone the repository, install the dependencies and then link the package.

```
you@yourmachine:~$ git clone https://gitlab.com/openclinical/pfanalyse.git
you@yourmachine:~$ cd pfanalyse
you@yourmachine:~$ npm install
you@yourmachine:~$ npm link
you@yourmachine:~$ pfanalyse

Usage: pfanalyse [options] <guideline_url> [decision_path] [data_url]

A command line tool to analyse proformajs decisions

Options:
  -d, --debug            Show debug information
  -u, --user <username>  username (for authenticating to labs.openclinical.net)
  -s, --show             Show decision PROforma (when viewing a decision)
  -V, --version          output the version number
  -h, --help             output usage information
```

PFAnalyse performs the following steps:

1. loads the guideline
2. grabs the decision, analyses it to find it's dependent data definitions
3. loads the csv file
4. checks the csv file to make sure that the first columns match the set of data definitions and the subsequent columns match the set of decision candidates
5. loads the data from each row of the csv into the extracted single decision model to see if the recommendation matches the suggested recommendation
6. returns a csv that has the following shape:

```csv
loaded	matched	flu	cold	other
true	false	NN	YN	NY
true	true	YY	NN	NN
```

Where the first "Y" or "N" indicates the value passed in the data file and the
second "Y" or "N" indicates whether the candidate is recommended by the decision model.
In the example one line matches the recommendation and the other does not.
The first case suggests a flu diagnosis where the data recorded a cold.

Note that if the csv file is ommitted, the tool performs steps 1 and 2 above and then outputs the headers for the csv it expects to injest and indicates the accepted values
for those columns.  Note that an asterix indicates a multi-valued data item which will accept array values.

## Examples

Try the following examples:

```
pfanalyse file://./etc/cold_or_flu.json
pfanalyse file://./etc/cold_or_flu.json cold_or_flu:diagnosis
pfanalyse file://./etc/cold_or_flu.json cold_or_flu:diagnosis file://./etc/cold_or_flu.tsv
pfanalyse file://./etc/cold_or_flu.json cold_or_flu:diagnosis file://./etc/cold_or_flu-noparse.tsv
pfanalyse file://./etc/dyndef.json exporttest:decision file://./etc/dyndef.tsv
pfanalyse https://labs.openclinical.net/api/v1/demo/cold_or_flu
pfanalyse file://./etc/issue1.json
pfanalyse file://./etc/issue1.json cold_or_flu:diagnosis2
pfanalyse file://./etc/issue1.json cold_or_flu:diagnosis2 file://./etc/issue1.json
```
