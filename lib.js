/*
PFAnalyse is a node CLI program for analysing proformajs decisions
Copyright (C) 2018 - Openclinical CIC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import { isUri } from "valid-url";
import { parse as _parse } from "url";
import superagent from "superagent";
const { get } = superagent;
import { Protocol, Enactment } from "@openclinical/proformajs";
import { readFile } from "fs";
import { parse } from "csv-parse";

// matches ``net_support("candidate1")`` or ``result_of(''cold_or_flu:diagnosis')`` in expressions
// contains an inner capture for ``candidate1`` or ``cold_or_flu:diagnosis`` clauses
const EXP_REGEX = "(\\w+)\\([\"|']([\\w+|:]+)[\"|']\\)";

export function getDecisionPaths(protocol) {
  return protocol
    .allTasks()
    .filter((task) => task instanceof Protocol.Decision)
    .map((dec) => dec.path());
}

// outputs decision paths in protocol
function showDecisions(protocol) {
  const decs = getDecisionPaths(protocol);
  if (decs.length > 0) {
    for (const dec of decs) {
      console.log("  " + dec);
    }
  } else {
    console.log("  no decisions found");
  }
}

// returns array of dependent data definition names
export function dependentDataDefs(decision) {
  const result = [];
  function merge(expression, referenced) {
    if (typeof expression != "undefined") {
      for (const def of decision.allDataDefinitions()) {
        if (expression.indexOf(def.name) > -1 && referenced.indexOf(def.name)==-1) {
          referenced.push(def.name);
          if (def.valueCondition && def.valueCondition != expression) {
            merge(def.valueCondition, referenced);
          }
        }
      }
      for (const def of referenced) {
        if (result.indexOf(def) == -1) {
          result.push(def);
        }
      }
    }
  }
  // check which dataDefinitions are referenced in the decision expressions
  for (const candidate of decision.candidates) {
    merge(candidate.recommendCondition, []);
    for (const argument of candidate.arguments) {
      merge(argument.activeCondition, []);
    }
  }
  return result;
}

export function dependentDecisions(decision) {
  const result = [];
  function merge(expression) {
    if (typeof expression != "undefined") {
      const matches = expression.match(new RegExp(EXP_REGEX, "g")) || [];
      if (matches) {
        for (const exp of matches) {
          // same rexp but not global so match returns capture groups
          const match = exp.match(new RegExp(EXP_REGEX));
          // e.g. match = [ "result_set("test:test1")", "result_set", "test:test1" ]
          if (
            match.length > 2 &&
            (match[1] == "result_of" || match[1] == "result_set") &&
            result.indexOf(match[2]) == -1
          ) {
            result.push(match[2]);
          }
        }
      }
    }
  }
  // check for expressions that include result_of or result_set
  for (const candidate of decision.candidates) {
    merge(candidate.recommendCondition);
    for (const argument of candidate.arguments) {
      merge(argument.activeCondition);
    }
  }
  return result;
}

// outputs expected column headers for csv
function showHeaders(decision) {
  for (const name of dependentDataDefs(decision)) {
    const def = decision.allDataDefinitions().find((dd) => dd.name == name);
    if (!def.valueCondition) {
      let text = def.name + " INPUT " + def.constructor.name;
      if (def.multiValued) {
        text = text + "*";
      }
      if (def.range) {
        text =
          text +
          " [" +
          def.range
            .map((val) =>
              typeof val === "object"
                ? JSON.stringify(val.value)
                : JSON.stringify(val),
            )
            .join(", ") +
          "]";
      }
      console.log(text);
    }
  }
  for (const cand of decision.candidates.map((cand) => cand.name)) {
    console.log(cand + " OUTPUT Boolean (i.e. isRecommended)");
  }
}

// outputs a Tallis-like version of the decision
// (more compact than the json version so easier to review)
function showPROforma(name, decision) {
  console.log("decision :: '" + name + "' ;");
  console.log("  caption :: '" + decision.caption + "' ;");
  console.log("  description :: '" + decision.description + "' ;");
  for (const cand of decision.candidates) {
    console.log("  candidate :: '" + cand.name + "' ;");
    for (const arg of cand.arguments) {
      console.log(
        "    argument :: " + arg.support + ", " + arg.activeCondition,
      );
      console.log("      caption :: '" + arg.caption + "' ;");
    }
    console.log("    recommendation :: " + cand.recommendCondition);
  }
}

// loads text from http(s): or file: url
export function loadUrl(url, log, callback) {
  if (isUri(url)) {
    const parsed = _parse(url);
    if (parsed.protocol == "http:" || parsed.protocol == "https:") {
      get(url).end((err, res) => {
        if (!err) {
          callback(err, res.text);
        } else {
          log("Are you sure that url is correct?", err);
          callback(err, res);
        }
      });
    } else if (parsed.protocol == "file:") {
      readFile(parsed.host + parsed.path, "utf8", (err, res) => {
        if (!err) {
          callback(err, res);
        } else {
          log("Are you sure that filepath is correct?\n" + err);
          callback(err, res);
        }
      });
    } else {
      log("Cannot handle url (needs file, http or https protocol): " + url);
    }
  } else {
    log("invalid url: " + url);
  }
}

export function extractDecision(protocol, path) {
  function rewriteExpression(expression) {
    expression = expression.replace(new RegExp(path, "g"), "decision");
    for (const decpath of decisions) {
      const matches = expression.match(new RegExp(EXP_REGEX, "g"));
      if (matches) {
        for (const exp of matches) {
          const matched = exp.match(new RegExp(EXP_REGEX));
          if (
            matched.length >= 2 &&
            (matched[1] == "result_of" || matched[1] == "result_set")
          ) {
            expression = expression.replace(exp, decpath.split(":").pop());
          }
        }
      }
    }
    return expression;
  }
  let original = protocol.getComponent(path);
  // isolate component: delete parent and rename decision
  let decision = Protocol.inflate(JSON.stringify(original));
  decision.name = "decision";
  delete decision.waitCondition;
  delete decision.preCondition;
  // copy in referenced data definitions
  let references = dependentDataDefs(original);
  for (const def of original.allDataDefinitions()) {
    if (references.indexOf(def.name) > -1) {
      const pojo = JSON.parse(JSON.stringify(def))
      decision.addDataDefinition(new Protocol[pojo.class](pojo));
    }
  }
  // create dd for any referenced decisions
  let decisions = dependentDecisions(original);
  for (const decpath of decisions) {
    const dec = protocol.getComponent(decpath);
    if (dec) {
      decision.addDataDefinition(
        new Protocol.Text({
          name: dec.name,
          multiValued: true,
          range: dec.candidates.map((cand) => cand.name),
        }),
      );
    }
  }
  // re-write expressions
  for (const candidate of decision.candidates) {
    if (candidate.recommendCondition) {
      candidate.recommendCondition = rewriteExpression(
        candidate.recommendCondition,
      );
    }
    for (const argument of candidate.arguments) {
      argument.activeCondition = rewriteExpression(argument.activeCondition);
    }
  }
  if (decision.isValid()) {
    return decision;
  } else {
    console.log(
      "extracted decision is invalid!",
      JSON.parse(JSON.stringify(decision)),
      decision.validate(),
    );
    throw new Error("unable to extract Decision");
  }
}

// expects val to be of the form { expected: Boolean, found: Boolean }
function showMatch(val) {
  return (val.expected ? "Y" : "N") + (val.found ? "Y" : "N");
}

export function processJSON(json, decision_path, data_url, log, show) {
  // TODO: catch protocol construction errors
  const protocol = new Protocol[json.class](json);
  if (decision_path) {
    const dec = protocol.getComponent(decision_path);
    if (dec) {
      if (dec instanceof Protocol.Decision) {
        // extract decision as single task named "decision" with needed dataDefinitions
        const atomic = extractDecision(protocol, decision_path);
        if (data_url) {
          loadUrl(data_url, log, (err, res) => {
            if (!err) {
              parse(
                res,
                { delimiter: "\t", columns: true, relax_quotes: true },
                (err, data) => {
                  if (!err) {
                    const cands = dec.candidates.map((cand) => cand.name);
                    const defs = atomic
                      .allDataDefinitions()
                      .filter((def) => typeof def.valueCondition == "undefined")
                      .map((def) => def.name);
                    // validate pass - has it got the right number of candidates / data values
                    if (
                      defs.length + cands.length !=
                      Object.keys(data[0]).length
                    ) {
                      console.log(
                        "Found " +
                          Object.keys(data[0]).length +
                          " cols in data file and " +
                          (defs.length + cands.length) +
                          " cols in decision (" +
                          defs.length +
                          " from data definititions and " +
                          cands.length +
                          " from candidates)",
                      );
                      console.log(
                        "Are you sure this data file is for this decision?",
                      );
                    } else {
                      // TODO: check for expected cols in data
                      console.log("loaded\tmatched\t" + cands.join("\t"));
                      // result is array on objects with shape:
                      // loaded: Boolean
                      // matches: Boolean
                      // candidates: { cand_name: { expected: Boolean, found: Boolean }}
                      let result = { loaded: false }; // default
                      for (const pass of data) {
                        let enactment = new Enactment({
                          start: true,
                          protocol: atomic,
                        });
                        let vals = {};
                        for (const key of Object.keys(pass)) {
                          if (
                            cands.indexOf(key.trim()) < 0 &&
                            pass[key].length > 0
                          ) {
                            const def = enactment.protocol
                              .allDataDefinitions()
                              .find((def) => def.name == key.trim());
                            vals[key.trim()] = Protocol[
                              def.constructor.name
                            ].inflate(pass[key]);
                          }
                        }
                        try {
                          enactment.set("decision", vals);
                          // data received, now let's examine the recommendations
                          result.candidates = {};
                          result.matched = true;
                          let confirmable = enactment.getStatus().confirmable;
                          for (const cand_path of Object.keys(confirmable)) {
                            let cand_name = cand_path.split(":")[1];
                            let expected = JSON.parse(pass[cand_name]);
                            let found = confirmable[cand_path].isRecommended;
                            result.candidates[cand_name] = {
                              expected: expected,
                              found: found,
                            };
                            if (expected != found) {
                              result.matched = false;
                            }
                          }
                          let text = "true\t" + result.matched;
                          for (const name of cands) {
                            text =
                              text + "\t" + showMatch(result.candidates[name]);
                          }
                          console.log(text);
                        } catch (e) {
                          // didnt load data successfully
                          console.log("problem adding data", e);
                        }
                      }
                    }
                  } else {
                    log("failed to parse csv", err);
                  }
                },
              );
            } else {
              log("unable to load data", err);
            }
          });
        } else {
          if (show) {
            showPROforma(decision_path.split(":").pop(), atomic);
          } else {
            // output headers for requested csv
            showHeaders(atomic);
          }
        }
      } else {
        log("invalid decision path: " + decision_path);
        console.log("available decision paths:");
        showDecisions(protocol);
      }
    } else {
      log("invalid component path: " + decision_path);
      console.log("available decision paths:");
      showDecisions(protocol);
    }
  } else {
    // list decisions in protocols
    log("available decision paths:");
    showDecisions(protocol);
  }
}
