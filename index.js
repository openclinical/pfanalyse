#!/usr/bin/env node

/*
PFAnalyse is a node CLI program for analysing proformajs decisions
Copyright (C) 2018 - Openclinical CIC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// see https://developer.atlassian.com/blog/2015/11/scripting-with-node/
import { Command } from "commander";
import { isUri } from "valid-url";
import { parse as _parse } from "url";
import superagent from "superagent";
const { get, post } = superagent;
import { readFile } from "fs/promises";
import { argv } from "process";
import { question } from "readline-sync";
import { processJSON } from "./lib.js";

const version = JSON.parse(
  await readFile(new URL("./package.json", import.meta.url)),
).version;

// labelled console output with toggleable verbosity
const log = function (text, err) {
  console.log("pfanalyse - " + text);
  if (err) {
    if (program.debug) {
      console.error(err);
    } else {
      console.error(err.message);
    }
  }
};

const program = new Command();
program
  .description("A command line tool to analyse proformajs decisions")
  .option("-d, --debug", "Show debug information")
  .option(
    "-u, --user <username>",
    "username (for authenticating to labs.openclinical.net)",
  )
  .option("-s, --show", "Show decision PROforma (when viewing a decision)")
  .arguments("<guideline_url> [decision_path] [data_url]")
  .version(version)
  .action(function (guideline_url, decision_path, data_url) {
    if (isUri(guideline_url)) {
      const url = _parse(guideline_url);
      if (url.protocol == "http:" || url.protocol == "https:") {
        // if username passed, ask for password and authenticate before requesting guideline
        if (program.opts.user != null) {
          if (url.hostname == "labs.openclinical.net") {
            const pwd = question("password? : ", { hideEchoBack: true });
            post("https://labs.openclinical.net/users/login")
              .set("Content-Type", "application/x-www-form-urlencoded")
              .send("username=" + program.opts.user)
              .send("password=" + pwd)
              .end((err, res) => {
                const json = JSON.parse(res.text);
                if (json.token) {
                  get(url)
                    .set("Authorization", "Bearer " + json.token)
                    .end((err, res) => {
                      if (!err) {
                        const json = JSON.parse(res.text);
                        processJSON(
                          json.data,
                          decision_path,
                          data_url,
                          log,
                          program.show,
                        );
                      } else {
                        log("Are you sure that url is correct?", err);
                      }
                    });
                } else {
                  log("unable to authenticate");
                }
              });
          } else {
            log("authentication only available for labs.openclinical.net");
          }
        } else {
          get(url).end((err, res) => {
            if (!err) {
              const json = JSON.parse(res.text);
              processJSON(
                json.data,
                decision_path,
                data_url,
                log,
                program.show,
              );
            } else {
              log("Are you sure that url is correct?", err);
            }
          });
        }
        // TODO: if username passed, logout
      } else if (url.protocol == "file:") {
        readFile(url.host + url.path, "utf8")
          .then((res) =>
            processJSON(
              JSON.parse(res),
              decision_path,
              data_url,
              log,
              program.show,
            ),
          )
          .catch((err) => log("Unexpected error:", err));
      } else {
        log("Cannot handle url: " + guideline_url);
      }
    } else {
      log("invalid url: " + guideline_url);
    }
  })
  .parse(argv);

if (!argv.slice(2).length) {
  program.outputHelp();
}
